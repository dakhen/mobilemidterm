import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
export default props => {
 
  return (
    <View>
      <Text style={styles.defaultFontSize}>Welcome To Your Dashbaord {props.route?.params?.user}</Text>
      <Text style={styles.defaultFontSize}> Thank for your testing on our app :) !!!   </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  defaultFontSize: {
    fontSize: 25,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});
